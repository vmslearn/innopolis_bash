#!/bin/bash

file1=$(cat ${1})
file2=$(cat ${2})

if [ $(($file1)) -ge $(($file2)) ]
then echo $(($file1))
else echo $(($file2))
fi
